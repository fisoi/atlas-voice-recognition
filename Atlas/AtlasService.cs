﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;

using System.Speech.Recognition;
using System.Speech.Synthesis;

namespace Atlas
{
    public partial class AtlasService : ServiceBase
    {
        public AtlasService()
        {
            InitializeComponent();
        }

        //LOG MANAGER OBJECT
        private LogManager log;

        //SPEECH RECOGNITION ENGINE
        private SpeechRecognitionEngine sRecognize;

        //SPEECH RECOGNITION TOGGLE
        private bool atlasEnabled = false;

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            //LOG MANAGER INITIALIZE
            log = new LogManager();

            //READ GRAMMAR
            Console.WriteLine("Reading dictionary ...");

            Choices slist = new Choices();
            string[] dictionary = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "dictionary/custom.txt");
            slist.Add(dictionary);

            Grammar gr = new Grammar(new GrammarBuilder(slist));
            Console.WriteLine("Grammar created ...");

            //START SPEECH RECOGNITION SERVICE
            try
            {
                sRecognize = new SpeechRecognitionEngine();

                sRecognize.RequestRecognizerUpdate();
                sRecognize.LoadGrammar(gr);

                sRecognize.SpeechRecognized += SRecognize_SpeechRecognized;
                sRecognize.SetInputToDefaultAudioDevice();

                sRecognize.RecognizeAsync(RecognizeMode.Multiple);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        //WORD RECOGNIZED
        private void SRecognize_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            //NOTIFY CONSOLE
            Console.WriteLine("-> " + e.Result.Text.ToString());

            if (e.Result.Text == "enable speech" && !atlasEnabled)
            {
                atlasEnabled = true;
                TextSpeaker.SpeakText("enabled");

            }
            else if(e.Result.Text == "disable speech" && atlasEnabled)
            {
                atlasEnabled = false;
                TextSpeaker.SpeakText("disabled");
            }

            if (atlasEnabled)
            {
                //TREAT CASE
                switch (e.Result.Text)
                {
                    case "mute":
                        KeysManager.Mute();
                        break;
                    case "volume up":
                        KeysManager.VolumeUp();
                        break;
                    case "volume down":
                        KeysManager.VolumeDown();
                        break;
                    case "volume up five":
                        for (int index = 0; index < 5; index++) KeysManager.VolumeUp();
                        break;
                    case "volume down five":
                        for (int index = 0; index < 5; index++) KeysManager.VolumeDown();
                        break;
                    case "volume up three":
                        for (int index = 0; index < 3; index++) KeysManager.VolumeUp();
                        break;
                    case "volume down three":
                        for (int index = 0; index < 3; index++) KeysManager.VolumeDown();
                        break;
                    case "stop":
                        if (scrollDown > 0) scrollDown = 0;
                        if (scrollUp > 0) scrollUp = 0;
                        break;
                    case "open virtual machine":
                        try
                        {
                            Process.Start(@"C:\Program Files (x86)\VMware\VMware Workstation\vmware.exe");
                        }
                        catch { }
                        break;
                    case "open file list":
                        System.Diagnostics.Process.Start("http://filelist.ro/browse.php");
                        break;
                    case "open facebook":
                        System.Diagnostics.Process.Start("https://www.facebook.com/");
                        break;
                    case "go down":
                        scrollDown = 1;

                        Thread scrollDownThread = new Thread(ScrollDownThread);
                        scrollDownThread.Start();
                        break;
                    case "go up":
                        scrollUp = 1;

                        Thread scrollUpThread = new Thread(ScrollUpThread);
                        scrollUpThread.Start();
                        break;
                    case "faster":
                        if (scrollUp > 0) scrollUp+=scrollUp;
                        if (scrollDown > 0) scrollDown+=scrollDown;
                        break;
                    case "time":
                        TextSpeaker.SpeakText((DateTime.Now.Hour > 12 ? DateTime.Now.Hour - 12 : DateTime.Now.Hour).ToString() 
                            + " and " 
                            + DateTime.Now.Minute.ToString() 
                            + " minutes");
                        break;
                }
            }
        }

        //SCROLL THREADS
        private int scrollDown, scrollUp;

        private void ScrollDownThread()
        {
            while (scrollDown > 0)
            {
                KeysManager.Scroll(-scrollDown);
                Thread.Sleep(10);
            }
        }

        private void ScrollUpThread()
        {
            while (scrollUp > 0)
            {
                KeysManager.Scroll(scrollUp);
                Thread.Sleep(10);
            }
        }


        protected override void OnStop()
        {
            TextSpeaker.Dispose();
        }
    }
}
