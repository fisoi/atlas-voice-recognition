﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas
{
    class LogManager
    {
        public LogManager()
        {
            try
            {
                //CREATE LOG DIRECTORY
                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "/log"))
                {
                    //the log dir was not found, create one
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "/log");
                }

                //CREATE LOG FILE
                File.Create(AppDomain.CurrentDomain.BaseDirectory + "/log/sesssion-" + DateTime.Now.Date.ToOADate() + "-" + DateTime.Now.ToFileTime().ToString() + ".txt");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
