﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;

namespace Atlas
{
    static class TextSpeaker
    {
        //SPEECH SYNTHETHIZATOR
        private static SpeechSynthesizer sSynth;

        
        public static void SpeakText(string text)
        {
            //START SPEECH SYNTH
            try
            {
                if (sSynth == null)
                {
                    sSynth = new SpeechSynthesizer();
                    sSynth.SelectVoiceByHints(VoiceGender.Female, VoiceAge.Adult);
                }

                PromptBuilder pBuilder = new PromptBuilder();
                pBuilder.AppendText(text);

                sSynth.Speak(pBuilder);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public static void Dispose()
        {
            if (sSynth != null)
            {
                sSynth.Dispose();
            }
        }
    }
}
